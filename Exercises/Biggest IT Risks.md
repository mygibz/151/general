# Die 10 grössten IT Risiken

1. Phishing (e.g. Identitätsdiebstähle, Malware, Ransomware)
2. Unsichere Passwörter (bei Admin UND User)
3. Social engineering
4. Cross Site Scripting / Injections (e.g. SQL/JavaScript)
5. Naturkatastrophen (e.g. Überflutung, Brand, ...)
6. Outdated Services/Software
7. Insider Jobs
8. "Bring your own device"
9. DDOS-Angriffe
10. Advanced Persistent Threats