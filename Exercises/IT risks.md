# IT Risiken

1. Insider Jobs, Diebstahl, Vandalismus
   - auch (menschlichen) Schwachstellen
     - phishing
   - DDOS
   - Hacking in general
2. Technische Fehler
   - Stromausfälle
   - Systemfehler
   - Datenverluste
   - wartungsbedingte Ausfälle
3. Höhere Gewalt
   - Katastrophenszenario (e.g. Überflutung am Server-Standort)
4. Technische Entwicklungen
   - hohen zeitlichen Digitalisierungsdruck
   - Umsetzung der gesetzlichen Anforderungen
   - Datenleaks, Diebstahl oder Sicherheitsverletzungen
5. Rechtsrisiken

Sources: [link11.com/...](https://www.link11.com/de/blog/it-sicherheit/it-risiken-bedrohungen-kennen-bewerten/)